// Simple product gallery for Zazzy
// Author(s): Martijn van der Veen
// 2016-08

var auth_headers = {
    Authorization: 'Token ' + window.ZAZZY_API2_TOKEN
}


window.GalleryContainer = React.createClass({
    productUrl: function(id) {
        var _id = (id && (id+"/") || "");
        return window.ZAZZY_API2 + 'products/' + _id;
    },
    loadProducts: function() {
        $.ajax({
            url: this.productUrl(), // FIXME: pagination
            headers: auth_headers,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({products: data.results});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error loading products:", status, err.toString());
            }.bind(this)
        });
    },
	getInitialState: function() {
		return {
            products: []
        };
    },
    componentDidMount: function() {
        this.loadProducts();
        // auto reload templates, for changes
        setInterval(this.loadProducts, this.props.pollInterval);
    },
    render: function() {
        var products = this.state.products.map(function(product, i) {
            return (
                <div className="item-block" key={product.name}>
                    <h3><a href={"product.html?id=" + product.id}
                           target="_blank">{product.name}</a></h3>
                    <p>{product.description}</p>

                    {Object.keys(product.images).map(function(key) {
                        if (product.images[key]) {
                            return <img src={product.images[key]} key={product.images[key]}/>
                        }
                    })}
                </div>
            );
        }.bind(this));
        return (
            <div>
                {products}
            </div>
        );
    }
});
