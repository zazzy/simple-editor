// Simple editor for Zazzy
// Author(s): Martijn van der Veen
// 2016-08

var auth_headers = {
    Authorization: 'Token ' + window.ZAZZY_API2_TOKEN
}


window.RenderableMixin = {
    /*
     * Asumes existence of:
     * - getRenderObjectUrl()
     * - handleIsRendering(isRendering)
     * - handleNewRender(url)
     *
     */
    productUrl: function(id) {
        var _id = (id && (id+"/") || "");
        return window.ZAZZY_API2 + 'products/' + _id;
    },
    itemUrl: function(id) {
        var _id = (id && (id+"/") || "");
        return window.ZAZZY_API2 + 'items/' + _id;
    },
    requestRender: function(extra_data) {
        this.handleIsRendering(true);
        $.ajax({
            url: this.getRenderObjectUrl(),
            method: "post",
            headers: auth_headers,
            data: JSON.stringify(extra_data || {}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                console.log("GOT RENDER", data)
                // 3. keep polling for result: GET this.productUrl(data.id)/renders/{key}
                this.pollForRender(data[0]);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error while rendering:", status, err.toString());
            }.bind(this)
        });
    },
    pollForRender: function(data) {
        // Keep polling for result
        $.ajax({
            url: data.self,
            method: "get",
            headers: auth_headers,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                console.log(data);
                if (data.status == "done") {
                    console.log("RENDER DONE", data);
                    this.handleNewRender(data.url);
                } else if (data.status == "error") {
                    console.log("RENDER ERROR", data);
                    this.handleNewRender("");
                } else {
                    setTimeout(function() { this.pollForRender(data); }.bind(this), 500);
                }
            }.bind(this)
        });
    },
};


window.EditorContainer = React.createClass({
    loadTemplates: function() {
        $.ajax({
            url: window.ZAZZY_API2 + 'templates/',
            headers: auth_headers,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.updateState({templates: data.results});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error loading templates:", status, err.toString());
            }.bind(this)
        });
    },
	handleTemplateChoice: function(e) {
		this.updateState({
            templateName: e.target.value,
            render: "",
            isRendering: false
        });
	},
    handleNewRender: function(url) {
        this.setState({render: url, isRendering: false});
    },
    handleIsRendering: function(isRendering) {
        this.setState({isRendering: isRendering});
    },
    updateState: function(update) {
        // update dependent variable this.state.template (is this the right way?)
        var template = this.state.templates.find(
			function(t) { return t.name == (update.templateName || this.state.templateName); }, this);
        update.template = template;
		this.setState(update);
    },
	getInitialState: function() {
		return {
            templates: [],
			template: null,
            templateName: null,
            render: false, // moved upstream
            isRendering: false,
        }
	},
    componentDidMount: function() {
        this.loadTemplates();
        // auto reload templates, for changes
        setInterval(this.loadTemplates, this.props.pollInterval);
    },
    render: function() {

        return (
			<div>
                <div className="left-block">
                    <h3>Zazzy Product Editor</h3>
                    <TemplateChooser templates={this.state.templates} templateName={this.state.templateName}
                                     handleTemplateChoice={this.handleTemplateChoice}/>

                    {this.state.template ?
                        <Editor template={this.state.template}
                                handleNewRender={this.handleNewRender}
                                handleIsRendering={this.handleIsRendering}
                                isRendering={this.state.isRendering}/>
                    :
                        <p>Choose a template above.</p>
                    }
                </div>

                <div className="right-block">
                {this.state.isRendering ?
                    "rendering.." // TODO set loading icon
                : this.state.render ?
                    <img src={this.state.render} />
                :
                    ""
                }
                </div>
			</div>
		);
    }
});


var TemplateChooser = React.createClass({
    render: function() {
		var templates = this.props.templates.map(function(template) {
            var classes = (template.name == this.props.templateName) ? "button-selected" : "";
			return (
				<button type="button" className={classes}
                        key={template.name} value={template.name}
                        onClick={this.props.handleTemplateChoice}>
					{template.name}
				</button>
			);
		}, this);
        return (
				<div className="templates">
					{templates}
				</div>
        );
    }
});


var Editor = React.createClass({
    mixins: [RenderableMixin],
	getInitialState: function() {
		return {
            // state == api product
            template: this.templateUrl(this.props.template.id),
            template_id: this.props.template.id,
            id: null,
            name: "Testing" + Math.round(Math.random()*1000),
            public: true,
            attributes: $.extend({}, this.props.template.options), // start fixed attributes
            options: {}, // may be moved to customisable options (checkbox)
            images: {},
        }
    },
    componentWillReceiveProps: function(props) {
        // Downstream Update: product may have changed
        if (props.template.id != this.state.template_id) {
            // template changed: try to update state intelligently
            console.log("TEMPLATE CHANGED");
            var attrs = this.state.attributes;
            var optns = this.state.options;
            for (var key in attrs) {
                if (!key in props.template.options)
                    delete attrs[key]; // only in old template
                else
                    attrs[key] = props.template.options[key];
            }
            for (var key in optns) {
                if (!key in props.template.options)
                    delete optns[key]; // only in old template
                else
                    optns[key] = props.template.options[key];
            }
            for (var key in props.template.options) {
                if (!(key in attrs) && !(key in optns)) {
                    attrs[key] = props.template.options[key]; // new
                }
            }
            this.setState({
                template_id: this.props.template.id,
                template: this.templateUrl(this.props.template.id),
                options: optns,
                attributes: attrs,
            });
        }
    },
    templateUrl: function(id) {
        return window.ZAZZY_API2 + 'templates/' + id + '/';
    },
    changeCustomisable: function(e) {
        var key = $(e.target).data('key');
        var value = $(e.target).is(':checked');
        var attrs = this.state.attributes;
        var optns = this.state.options;
        if (value) {
            // move from attributes to options
            var item = attrs[key] || optns[key];
            delete attrs[key];
            optns[key] = item;
        } else {
            // move from options to attributes
            var item = optns[key] || attrs[key];
            delete optns[key];
            attrs[key] = item;
        }
        this.setState({options: optns, attributes: attrs});
    },
    changeOption: function(e) {
        // change template option
        var key = e.target.name;
        var value = e.target.value;
        if (key in this.state.attributes) {
            // change as product attribute value
            var attrs = this.state.attributes;
            if (typeof attrs[key] === 'object') {
                attrs[key].default = value;
            } else {
                attrs[key] = value;
            }
            this.setState({attributes: attrs});
        } else {
            // change as product option value
            var optns = this.state.options;
            if (typeof optns[key] === 'object') {
                optns[key].default = value;
            } else {
                optns[key] = value;
            }
            this.setState({options: optns});
        }
    },
    handleNewRender: function(url) {
        this.props.handleNewRender(url);
    },
    handleIsRendering: function(isRendering) {
        this.props.handleIsRendering(isRendering);
    },
    handlePossibleEnter: function(e) {
        if ((e.which || e.keyCode) == 13) {
            $('#showResult').click();
        }
    },
    getRenderObjectUrl: function() {
        return this.productUrl(this.state.id) + 'renders/';
    },
    sync: function(callback) {
        // Synchronise editor data with API product
        console.log("SYNC TO API");
        // cleaning data
        var data = $.extend(true, {}, this.state); // deep copy
        for (var key in data.attributes) {
            var attr = data.attributes[key];
            data.attributes[key] = (typeof attr === 'object') && (attr.default || "") || attr;
        };
        // save (create or send patch)
        $.ajax({
            url: this.productUrl(this.state.id),
            method: (this.state.id && "patch" || "post"),
            headers: auth_headers,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({id: data.id});
                if (callback)
                    callback(data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error saving product:", status, err.toString());
            }.bind(this)
        });
    },
    showResult: function(e) {
        // Get render and show
        this.sync(function(data) {
            var extra_data = {};
            this.requestRender(extra_data);
        }.bind(this));
    },
    showResultCycles: function(e) {
        // Get render and show
        this.sync(function(data) {
            var extra_data = {
                'render-engine': 'cycles',
                'render-scene': 'melanie_{product}',
                'resolution': [500, 500]
            };
            this.requestRender(extra_data);
        }.bind(this));
    },
	render: function() {
        // show template attributes
		var templateAttributes = this.props.template.attributes;
		var attributes = Object.keys(templateAttributes).map(function(key) {
			var uniqKey = "attribute-" + key;
			return (
				<Attribute key={uniqKey} name={key} value={templateAttributes[key]}/>
			)
		});
        // show template options (might become product attributes or product options)
		var templateOptions = $.extend({}, this.state.options, this.state.attributes);
		var options = Object.keys(templateOptions).map(function(key) {
			var uniqKey = "option-" + key;
            var isCustomisable = key in this.state.options;
			return (
				<Option key={uniqKey} name={key} value={templateOptions[key]}
                        changeOption={this.changeOption} handlePossibleEnter={this.handlePossibleEnter}
                        isCustomisable={isCustomisable} changeCustomisable={this.changeCustomisable}/>
			)
		}, this);
		return (
			<div className="editorContainer">
				<h3>Design a {this.props.template.name}</h3>
                <p>{this.props.template.description}</p>

				{attributes}

				{options}

				<button type="button" id="showResult"
                        disabled={this.props.isRendering ? "disabled" : ""}
                        onClick={this.showResult}>
                    {this.props.isRendering ? "Rendering..." : "Show Result"}
				</button>
				<button type="button" id="showResultCycles"
                        disabled={this.props.isRendering ? "disabled" : ""}
                        onClick={this.showResultCycles}>
                    {this.props.isRendering ? "Rendering..." : "Show Cycles Result"}
				</button>

			</div>
		);
	}
});

window.Attribute = React.createClass({
    render: function() {
        return (
            <div>
                <div className="keyName">{this.props.name}</div>
                <div className="keyValue">{this.props.value}</div>
            </div>
        );
    }
});

window.Option = React.createClass({
    /* Assuming option has one of these forms:
     * - a value: "text", 123
     * - an object with keys: {
     *     "default":   "default value",                // required
     *     "choices":   ["First", "Second", "Third"]    // optional, for dropdowns
     * }
     */
    render: function() {
        var value = this.props.value;
        var type = typeof value;
        if (typeof this.props.value === 'object') {
            value = this.props.value.default || (this.props.value.choices && this.props.value.choices[0]);
            type = typeof value;
            if (this.props.value.choices)
                type = "dropdown";
        }
        if (String(value).match("<png>.*</png>")) {
            type = "canvas";
        }
        //console.log(this.props.value, value, type, ui);
        return (
            <div>
                <div className="keyName">{this.props.name}</div>
                <div className="keyValue">
                    {type == "string" ?
                        <input type="text"
                               name={this.props.name} value={value}
                               onChange={this.props.changeOption}
                               onKeyPress={this.props.handlePossibleEnter}
                               autoFocus={true}/>
                    : type == "dropdown" ?
                        <select name={this.props.name}
                                defaultValue={this.props.value.default}
                                onChange={this.props.changeOption}>
                            {this.props.value.choices.map(function(choice, i) {
                                return <option value={choice} key={i}>{choice}</option>
                            }.bind(this))}
                        </select>
                    : type == "canvas" ?
                        <Canvas name={this.props.name} value={value}
                                handleUpdate={this.props.changeOption}/>
                    :
                        <p>{value}</p>
                    }
                    {this.props.changeCustomisable ?
                        <label>
                            <input type="checkbox"
                                   data-key={this.props.name}
                                   checked={this.props.isCustomisable}
                                   onChange={this.props.changeCustomisable}/>
                            Allow customisation
                        </label>
                    :
                        ""
                    }
                </div>
            </div>
        );
    }
});


var Canvas = React.createClass({
    openUpload: function(e) {
        $('#fileUpload' + this.props.name).click();
    },
    handleUpload: function(e) {
        var file = $('#fileUpload' + this.props.name)[0].files[0];
        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onload = function(e) {
            img.src = e.target.result;
            this.drawImage(img);
        }.bind(this);
        reader.readAsDataURL(file);
    },
    drawImage: function(img) {
        // Draw image (<img> element) on canvas
        var canvas = $('#canvas' + this.props.name)[0];
        var ctx = canvas.getContext("2d");
        var width = canvas.width;
        var height = canvas.height;
        ctx.drawImage(img, 0, 0, width, height);
        this.forceUpdate();
        var value = "<png>" + canvas.toDataURL().substr(22) + "</png>";
        //console.log("UPDATING TO VALUE", value);
        $(canvas).attr('value', value);
        this.props.handleUpdate({target: {name: this.props.name, value: value}});
    },
    render: function() {
        return (
            <div>
                <canvas id={"canvas" + this.props.name} width={320} height={320}
                        name={this.props.name} value=""></canvas>
                <div>
                    <button className="pull-right" type="button"
                            onClick={this.openUpload}>Upload</button>
                    <input type="file" id={"fileUpload" + this.props.name}
                           onChange={this.handleUpload} />
                </div>
            </div>
        )
    }
});
