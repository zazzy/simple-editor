// Simple product page for Zazzy
// Author(s): Martijn van der Veen
// 2016-08

var auth_headers = {
    Authorization: 'Token ' + window.ZAZZY_API2_TOKEN
}

var auth_headers_backend = {
    // NOTE: for backend use only (don't expose token to end user)
    Authorization: 'Token ' + window.ZAZZY_API2_TOKEN_BACKEND
}


window.ProductContainer = React.createClass({
    mixins: [window.RenderableMixin],
    loadProduct: function() {
        $.ajax({
            url: window.ZAZZY_API2 + 'products/' + this.state.product_id + '/',
            headers: auth_headers,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({
                    product: this.productUrl(data.id),
                    product_data: data,
                    product_id: data.id,
                    attributes: data.options
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error loading product:", status, err.toString());
            }.bind(this)
        });
    },
	getInitialState: function() {
		// get ID from url
		var product_id = location.search.split('id=')[1];
        return {
            product_data: {},
            product: null,
            product_id: product_id,
            id: null,
            attributes: {},

            // other props
            render: "",
            isRendering: false,
        };
    },
    componentDidMount: function() {
        if (!this.state.product_id) {
            alert("No product id given! Add as GET parameter: ?id={id}");
            return;
        }
        this.loadProduct();
    },
    changeOption: function(e) {
        // change item option
        var key = e.target.name;
        var value = e.target.value;
        var attrs = this.state.attributes;
        if (typeof attrs[key] === 'object') {
            attrs[key].default = value;
        } else {
            attrs[key] = value;
        }
        this.setState({attributes: attrs});
    },
    getRenderObjectUrl: function() {
        return this.itemUrl(this.state.id) + 'renders/';
    },
    orderUrl: function(id) {
        return window.ZAZZY_API2 + 'orders/';
    },
    handleIsRendering: function(isRendering) {
        this.setState({isRendering: isRendering});
    },
    handleNewRender: function(url) {
        this.setState({render: url, isRendering: false});
    },
    handlePossibleEnter: function(e) {
        if ((e.which || e.keyCode) == 13) {
            $('#showResult').click(); // TODO
        }
    },
    sync: function(callback) {
        // Synchronise editor data with API product
        console.log("SYNC TO API");
        // cleaning data
        var data = $.extend(true, {}, this.state); // deep copy
        for (var key in data.attributes) {
            var attr = data.attributes[key];
            data.attributes[key] = (typeof attr === 'object') && (attr.default || "") || attr;
        };
        console.log("DATA IS NOW", data);
        // save (create or send patch)
        $.ajax({
            url: this.itemUrl(this.state.id),
            method: (this.state.id && "patch" || "post"),
            headers: auth_headers,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({id: data.id});
                if (callback)
                    callback(data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("[!!] error saving item:", status, err.toString());
            }.bind(this)
        });
    },
    showResult: function() {
        // Get render and show
        this.sync(function(data) {
            var extra_data = {};
            this.requestRender(extra_data);
        }.bind(this));
    },
    buy: function() {
        console.log("buying");
        this.sync(function(data) {
            // Simulating backend call
            var order = {
                'firstname': "Zazzy",
                'lastname': "HQ",
                'email': "angels@zazzy.co",
                'address': "Herengracht 182",
                'postcode': "1016 BR",
                'city': "Amsterdam",
                'country': "Netherlands",
                'price_total': "0.42",
                'price_purchase': "0.42",
                'items': [
                    {'item': this.itemUrl(this.state.id), 'quantity': 1}
                ]
            };
            $.ajax({
                // NOTE: this call should happen from a secured backend only
                //       (not from the frontend); you'll be charged for production
                url: this.orderUrl(this.state.id),
                method: "POST",
                headers: auth_headers_backend,
                data: JSON.stringify(order),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                success: function(data) {
                    console.log("ORDER PLACED:", data);
                    alert("Order placed by " + data.owner + "!\nSee js console for details.");
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error("[!!] error saving order:", status, err.toString());
                }.bind(this)
            });
        }.bind(this));
    },
    render: function() {
        //console.log(this.state);
		var options = Object.keys(this.state.attributes).map(function(key) {
			var uniqKey = "option-" + key;
			return (
				<Option key={uniqKey} name={key} value={this.state.attributes[key]}
                        changeOption={this.changeOption} handlePossibleEnter={this.handlePossibleEnter}
                        isCustomisable={false} changeCustomisable={null}/>
			);
		}, this);
        var imgs = this.state.product_data.images;
        var images = "";
        if (imgs) {
            images = Object.keys(imgs).map(function(key) {
                var uniqKey = "images-" + key;
                return (
                    <img src={imgs[key]} key={imgs[key]}/>
                );
            }, this);
        }
        return (
            <div>
                <div className="left-block">

                {images}

                {options}

                {options.length ?
                    <button type="button" id="showResult" className="inline-block"
                            disabled={this.props.isRendering ? "disabled" : ""}
                            onClick={this.showResult}>
                        {this.props.isRendering ? "Rendering..." : "Show Result"}
                    </button>
                :
                    ""
                }

                    <button type="button" id="buy" className="inline-block"
                            disabled={this.state.isRendering ? "disabled" : ""}
                            onClick={this.buy}>
                        Buy
                    </button>

                </div>

                <div className="right-block">
                {this.state.isRendering ?
                    "rendering.." // TODO set loading icon
                : this.state.render ?
                    <img src={this.state.render} />
                :
                    ""
                }
                </div>
            </div>
        );
    }
});
