This is an example implementation of the Zazzy Partner API v2.

This repository contains:

- `editor.html`: use templates to create products for sale
- `gallery.html`: overview of all public products available for sale
- `product.html?id={id}`: product sale page. Might contain
  customisable parameters (if the product contains `options`) with
  corresponding editor UI elements.

Open `www/{file}.html` in your browser. If you get CORS errors
(e.g., Chrome), try running with a local webserver:
`python -m SimpleHTTPServer`

**NOTE: PREVIEW ONLY**: the API v2 is in the final phase of development,
but might see considerable improvements and (possibly) breaking changes
over the next couple of weeks.
